import React from 'react';
import ProductList from "./ProductList";


class Main extends React.Component {

    constructor(props) {
        super(props);

    }

    render() {
        return (

            <div className="main">
                <div className="main-wrapper">
                    <ProductList categoryID={this.props.categoryID}/>
                    {/*<div className="card">*/}
                    {/*    <img src="https://podkapot.com.ua/image-cache/?h=300&a=3&f=static-files%2Fimg%2Fgoods%2F6a%2F0f%2Fradiator-4296.jpg" className="card-img-top" alt="Радиатор" />*/}
                    {/*        <div className="card-body">*/}
                    {/*            <h5 className="card-title">Радиатор</h5>*/}
                    {/*            <p className="card-text">100$</p>*/}
                    {/*            <a href="#" className="btn btn-primary">Купить</a>*/}
                    {/*        </div>*/}
                    {/*</div>*/}
                    {/*<div className="card">*/}
                    {/*    <img src="https://podkapot.com.ua/image-cache/?h=300&a=3&f=static-files%2Fimg%2Fgoods%2Fe9%2F0b%2Fdveri-i-bagazhnik-7650.png" className="card-img-top" alt="Радиатор" />*/}
                    {/*    <div className="card-body">*/}
                    {/*        <h5 className="card-title">Дверь</h5>*/}
                    {/*        <p className="card-text">200$</p>*/}
                    {/*        <a href="#" className="btn btn-primary">Купить</a>*/}
                    {/*    </div>*/}
                    {/*</div>*/}
                    {/*<div className="card">*/}
                    {/*    <img src="https://podkapot.com.ua/image-cache/?h=300&a=3&f=static-files%2Fimg%2Fgoods%2F9a%2F37%2Fvnutrennie-komponenty-kuzova-3440.png" className="card-img-top" alt="Радиатор" />*/}
                    {/*    <div className="card-body">*/}
                    {/*        <h5 className="card-title">Коврики</h5>*/}
                    {/*        <p className="card-text">25$</p>*/}
                    {/*        <a href="#" className="btn btn-primary">Купить</a>*/}
                    {/*    </div>*/}
                    {/*</div>*/}
                    {/*<div className="card">*/}
                    {/*    <img src="https://podkapot.com.ua/image-cache/?h=300&a=3&f=static-files%2Fimg%2Fgoods%2F14%2F69%2Fdvigatel-6113.png" className="card-img-top" alt="Радиатор" />*/}
                    {/*    <div className="card-body">*/}
                    {/*        <h5 className="card-title">Двигатель</h5>*/}
                    {/*        <p className="card-text">1500$</p>*/}
                    {/*        <a href="#" className="btn btn-primary">Купить</a>*/}
                    {/*    </div>*/}
                    {/*</div>*/}
                    {/*<div className="card">*/}
                    {/*    <img src="https://podkapot.com.ua/image-cache/?h=300&a=3&f=static-files%2Fimg%2Fgoods%2F46%2Fe4%2Fkolesa-i-shiny-7676.png" className="card-img-top" alt="Радиатор" />*/}
                    {/*    <div className="card-body">*/}
                    {/*        <h5 className="card-title">Комплект колёс</h5>*/}
                    {/*        <p className="card-text">450$</p>*/}
                    {/*        <a href="#" className="btn btn-primary">Купить</a>*/}
                    {/*    </div>*/}
                    {/*</div>*/}
                    {/*<div className="card">*/}
                    {/*    <img src="https://podkapot.com.ua/image-cache/?h=300&a=3&f=static-files%2Fimg%2Fgoods%2Fef%2Ffd%2Fglushitel200-5613.jpg" className="card-img-top" alt="Радиатор" />*/}
                    {/*    <div className="card-body">*/}
                    {/*        <h5 className="card-title">Глушитель</h5>*/}
                    {/*        <p className="card-text">30$</p>*/}
                    {/*        <a href="#" className="btn btn-primary">Купить</a>*/}
                    {/*    </div>*/}
                    {/*</div>*/}
                </div>
            </div>
        );
    }
}

export default Main;

