import React from 'react';
import ReactDOM from 'react-dom';
import {Link} from "react-router-dom";

function Header() {
    return (
        <div className="header">
            <div className="wrapper">
                <div className="header_top">
                    <a href="#"><img src="images/carpartlogo.png" alt="logo" className="header_logo"/></a>

                    <nav className="header_menu">
                        <ul className="menu">
                            <li className="menu_item"><Link to ="/">Главная</Link></li>
                            <li className="menu_item"><Link to ="/warranty">Гарантии</Link></li>
                            <li className="menu_item"><Link to ="/about">О нас</Link></li>
                            <li className="menu_item"><Link to ="/contacts">Контакты</Link></li>
                        </ul>
                    </nav>
                </div>
                <div className="header_bottom">
                    <h1 className="title">Автозапчасти для иномарок</h1>
                    <a href="#" className="download_link">Скачать наш прайс</a>
                    <form action="#" method="get" className="search_form">
                        <input type="search" name="search" placeholder="Поиск по сайту" className="search"/>
                    </form>
                </div>
            </div>
        </div>
    );
}

export default Header;

