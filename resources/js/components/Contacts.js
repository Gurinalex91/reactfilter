import React from "react";

function Contacts() {
    return (
        <div>
        <div className="contacts">
            <div className="container">
                <h1>Контакты</h1>
                <div className="content">
                    <p>Авторазборка CarPart находится в г. Владимир-Волынский, представительства имеются в других городах Украины,
                        Европы.</p>
                    <ul>
                        <p>Наши телефоны:</p>
                        <li>+380937527178</li>
                        <li>+380999881152</li>
                        <li>+380979451119</li>
                    </ul>

                    <p>Для связи с нами звоните менеджерам или оставьте запрос на запчасть (заполните форму ниже).</p>

                    <p><u><strong>Время работы:</strong></u> Понедельник —&nbsp;Пятница, с 9.00 до 18.00. В нерабочее
                        время
                        заявки принимаются через форму на сайте (смотрите ниже).</p>
                </div>
            </div>
        </div>

     <div className="container0">
         <div className="container">

             <div className="content">
                <div className="blockform">
                     <div className="form">
                         <form method="post" action="#">
                                 <h4>
                                     Запрос на запчасть
                                 </h4>
                                 <p>
                                     <label htmlFor="name">
                                         Ваше имя
                                     </label>
                                     <input type="name" name="name" placeholder="Имя" value="" />
                                 </p>
                                 <p>
                                     <label htmlFor="email">
                                         Ваш email
                                     </label>
                                     <input type="email" name="email" placeholder="E-mail" value="" />
                                 </p>
                                 <p>
                                     <label htmlFor="phone">
                                         Ваш номер телефона <span>*</span>
                                     </label>
                                     <input type="phone" name="phone" placeholder="Телефон *" value="" required="" />

                                 </p>
                                 <p>
                                     <label htmlFor="textarea">
                                         Укажите интересующую Вас запчасть и данные автомобиля (марка, модель автомобиля,
                                         год
                                         выпуска, объем двигателя и т.д.)
                                     </label>
                                     <textarea name="textarea" rows="5"
                                               placeholder="Введите данные для запроса на запчасть"
                                               value=""></textarea>
                                 </p>
                             <p>
                                 <input className="submit" type="submit" value="Отправить заявку" />
                             </p>
                         </form>
                     </div>
                </div>
             </div>
         </div>
     </div>

    <div className="links">
        <div className="container">
            <ul className="reviews">
                <li>
                    <a href="//google.com/search?q=carpart.com.ua" rel="nofollow"
                       className="inner">
                                <span className="img">
                                    <img src="http://zhelezyaka.com.ua/tpl/images/googlelogo-min.png" alt="" />
                                </span>
                        <span className="caption">Страница компании в GOOGLE</span>
                    </a>
                </li>
                <li>
                    <a href="//point.autoua.net/tochki/7964-carpart/" rel="nofollow" className="inner">
                                <span className="img">
                                    <img src="http://zhelezyaka.com.ua/tpl/images/autoualogo-min.png" alt="" />
                                </span>
                        <span className="caption">Мы на сайте автоклуба АвтоUA</span>
                    </a>
                </li>
                <li>
                    <a href="//www.instagram.com/carpart_ua/" rel="nofollow" className="inner">
                                <span className="img">
                                    <img src="http://zhelezyaka.com.ua/tpl/images/instagramlogo-min.png" alt="" />
                                </span>
                        <span className="caption">Carpart в Instagram</span>
                    </a>
                </li>
            </ul>
        </div>
    </div>
    </div>
);
}

export default Contacts;
