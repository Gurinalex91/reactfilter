import React from "react";

class CategoryList extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            categoryID: 0,
            categories:[]
        }
    }

    componentDidMount() {
        fetch("/api/categories")
            .then(res => res.json())
            .then(
                (result) => {

                    console.log(result);

                    this.setState({
                        //    isLoaded: true,
                        categories: result
                    });
                },
                   (error) => {
                    this.setState({
                        isLoaded: true,
                        error
                    });
                }
            )
    }

    render(){
        return (
        <div className="category-list">
            {/*<button onClick={() => { this.props.updateCategory(2)} }>Start</button>*/}
            <ul>
                {this.state.categories.map(item => (
                    <li key={item.name} onClick={ () => {this.props.updateCategory(item.id)}}>
                        {item.name}
                    </li>
                ))}
            </ul>
        </div>
        )
    }
}
export default CategoryList;
