import React from 'react';


function Footer() {
    return (
        <div className="footer">
            <div className="footer-wrapper">
                <p>© 2016 - 2021, All Right Reserved</p>
            </div>
        </div>
    );
}

export default Footer;

