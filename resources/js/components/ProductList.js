import React from "react";


class ProductList extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            products:[]
        }
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        console.dir("Prev props:" + prevProps.categoryID);
        console.dir("Current props:" + this.props.categoryID);


        if(prevProps.categoryID != this.props.categoryID){
            // AJAX
            fetch("/api/products/" + this.props.categoryID)
                .then(res => res.json())
                .then(
                    (result) => {
                        this.setState({
                            //    isLoaded: true,
                            products: result
                        });
                    },
                    (error) => {
                        this.setState({
                            isLoaded: true,
                            error
                        });
                    }
                )
        }
    }

    componentDidMount() {

        console.log(this.props.categoryID);

        fetch("/api/products/")
            .then(res => res.json())
            .then(
                (result) => {

                    console.log(result);

                    this.setState({
                        //    isLoaded: true,
                        products: result
                    });
                },
                (error) => {
                    this.setState({
                        isLoaded: true,
                        error
                    });
                }
            )
    }

    render(){
        console.log( "Category ID:  " + this.props.categoryID);

        return (
            <div>
                <ul className="item-list">

                    {this.state.products.map(item => (
                        <li key={item.name} onClick={ () => {this.props.updateCategory(item.id)}}>
                            <div className="card">
                                <img src="https://podkapot.com.ua/image-cache/?h=300&a=3&f=static-files%2Fimg%2Fgoods%2F6a%2F0f%2Fradiator-4296.jpg" className="card-img-top" alt="Радиатор" />
                                <div className="card-body">
                                    <h5 className="card-title">{item.name}</h5>
                                    <p className="card-text">100$</p>
                                    <a href="#" className="btn btn-primary">Купить</a>
                                </div>
                            </div>

                        </li>
                    ))}
                </ul>
            </div>
        )
    }
}
export default ProductList;
