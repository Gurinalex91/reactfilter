import React from "react";
import ReactDOM from "react-dom";
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Link
} from "react-router-dom";
import Header from "./Header";
import Footer from "./Footer";
import About from "./About";
import Warranty from "./Warranty"
import Contacts from "./Contacts"
import Sidebar from "./Sidebar";
import Main from "./Main";


class App extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            categoryID: 0,
            defaultValue: [0, 100],
        }
    }

    updateCategory = (id) => {
        this.setState({
           categoryID : id
        });

        console.log("Set " +id+ " category");
    }


    render(){

        return (
        <Router>
            <Header />
            <Switch>
                <Route path="/about">
                    <About />
                </Route>
                <Route path="/warranty">
                    <Warranty />
                </Route>
                <Route path="/contacts">
                    <Contacts />
                </Route>
                <Route path="/">
                    <div className="content-container">
                        <Sidebar updateCategory={this.updateCategory}/>
                        <Main categoryID={this.state.categoryID} />
                    </div>
                </Route>
            </Switch>

            <Footer />
        </Router>
        )
    }
}
export default App;

if (document.getElementById('app')) {
    ReactDOM.render(<App />, document.getElementById('app'));
}
